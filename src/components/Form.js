import { useDispatch, useSelector } from 'react-redux'
import { addTodo, updateFilterStatus } from '../features/todoSlice'
import { useState } from 'react'

const Form = () => {
	const [inputValue, setInputValue] = useState('')

	const dispatch = useDispatch()

	const onSubmit = (e) => {
		e.preventDefault()
		dispatch(addTodo({ title: inputValue }))
		setInputValue('')
	}

	return (
		<form onSubmit={onSubmit}>
			<input
				onChange={(e) => setInputValue(e.target.value)}
				value={inputValue}
				type='text'
				className='todo-input'
				placeholder='Add your todo here...'
			/>
			<button className='todo-button' type='submit'>
				<i className='fas fa-plus-square'></i>
			</button>
		</form>
	)
}
export default Form
