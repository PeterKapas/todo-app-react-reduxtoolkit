import { deleteTodo, toogleCompleted } from '../features/todoSlice'
import { useDispatch } from 'react-redux'

const Todo = ({ title, id, completed }) => {
	const dispatch = useDispatch()
	const handleDelete = () => dispatch(deleteTodo({ id: id }))
	const handleCompleted = () => {
		dispatch(toogleCompleted({ id: id, completed: !completed }))
	}

	return (
		<div className={`todo ${completed ? 'complete' : ''}`}>
			<li>{title}</li>

			<button onClick={handleCompleted} className='complete-btn'>
				<i className='fas fa-check'></i>
			</button>
			<button onClick={handleDelete} className='trash-btn'>
				<i className='fas fa-trash'></i>
			</button>
		</div>
	)
}
export default Todo
