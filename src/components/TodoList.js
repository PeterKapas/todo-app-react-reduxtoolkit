import Todo from './Todo'
import { useSelector } from 'react-redux'

const TodoList = () => {
	const todoList = useSelector((state) => state.todosred.value)
	return (
		<div className='todo-container'>
			<ul className='todo-list'>
				{todoList.map((todoobject) => (
					<Todo
						id={todoobject.id}
						title={todoobject.title}
						completed={todoobject.completed}
					/>
				))}
			</ul>
		</div>
	)
}
export default TodoList
