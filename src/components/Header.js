import { useSelector } from 'react-redux'

const Header = () => {
	const globalState = useSelector((state) => state.todosred.value)
	const globalCompleted = globalState.filter((todo) => todo.completed) //try with useMemo later
	const globalIncompleted = globalState.filter((todo) => !todo.completed)
	return (
		<header>
			<h1>Peter's Todo List</h1>
			<h4>All todos: {globalState.length} </h4>
			<h5>Incompleted todos: {globalIncompleted.length} </h5>
			<h5>Completed todos: {globalCompleted.length} </h5>
		</header>
	)
}
export default Header

/* 
Const countIncompletedTodos = useMemo(() => {
todos.filter((todo) => {
  !todo.complete
  })
  }, [todos]) */
