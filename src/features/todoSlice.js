import { createSlice } from '@reduxjs/toolkit'
import { nanoid } from '@reduxjs/toolkit'

const initialState = {
	value: [
		{ id: 1, title: 'todo1', completed: false },
		{ id: 2, title: 'todo2', completed: false },
		{ id: 3, title: 'todo3', completed: false },
	],
}

const todoSlice = createSlice({
	name: 'todos1',
	initialState,
	reducers: {
		addTodo: (state, action) => {
			const newTodo = {
				id: nanoid(),
				title: action.payload.title,
				completed: false,
			}
			state.value.push(newTodo)
		},
		deleteTodo: (state, action) => {
			state.value = state.value.filter((todo) => todo.id !== action.payload.id)
		},
		toogleCompleted: (state, action) => {
			const findTodoIndex = state.value.findIndex(
				(todo) => todo.id === action.payload.id
			)
			state.value[findTodoIndex].completed = action.payload.completed
		},
	},
})

export const { addTodo, deleteTodo, toogleCompleted } = todoSlice.actions
export default todoSlice.reducer
