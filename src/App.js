import Header from './components/Header'
import Form from './components/Form'

import TodoList from './components/TodoList'

const App = () => {
	return (
		<div className='App'>
			<Header />
			<Form />
			<TodoList />
		</div>
	)
}
export default App
